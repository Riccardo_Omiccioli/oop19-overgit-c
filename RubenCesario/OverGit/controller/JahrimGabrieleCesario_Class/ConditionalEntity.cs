﻿/* 
 * Translation by Jahrim Gabriele Cesario 
 */
namespace ConsoleApp
{

    public interface IConditionalEntity
    {
        //METHODS
        /** Returns true if this entity still exists. */
        bool IsExisting();
    }
    //DEFAULT METHODS
    public static class IConditionalEntityHelper
    {
        /** Returns true if this entity doesn't exist anymore. */
        public static bool IsntExisting(this IConditionalEntity entity) => !entity.IsExisting();
    }

    public class ConditionalEntity : IConditionalEntity
    {
        //FIELDS
        private readonly ExistenceCondition existenceCondition;

        //METHODS
        public ConditionalEntity(ExistenceCondition condition)
        {
            this.existenceCondition = condition;
        }

        public bool IsExisting()
        {
            return this.existenceCondition();
        }

        //DELEGATES
        public delegate bool ExistenceCondition();
    }

}
