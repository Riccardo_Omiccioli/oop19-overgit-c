﻿/* 
 * Translation by Jahrim Gabriele Cesario 
 */
using System;

namespace ConsoleApp
{

    public interface IRotatingEntity : ICollidableEntity
    {
        //PROPERTIES
        double Rotation { get; set; }

        //METHODS
        /** Rotates this entity of a certain angle, anti-clockwise. */
        void RotateAnticlockwise(double angle);
    }
    //DEFAULT METHODS
    public static class IRotatingEntityHelper
    {
        /** Rotates this entity of a certain angle, clockwise. */
        public static void RotateClockwise(this IRotatingEntity entity, double angle) => entity.RotateAnticlockwise(-angle);
    }

    public class RotatingEntity : CollidableEntity, IRotatingEntity
    {
        //FIELDS
        private static readonly double DEFAULT_ANGLE = 0;

        private double radiantAngle;

        //PROPERTIES
        public double Rotation
        {
            get { return this.radiantAngle; }
            set { this.radiantAngle = value; }
        }

        //METHODS
        public RotatingEntity() : base()
        {
            this.radiantAngle = RotatingEntity.DEFAULT_ANGLE;
        }
        public RotatingEntity(Point2D position, double radius) : this(position, radius, RotatingEntity.DEFAULT_ANGLE) { }
        public RotatingEntity(Point2D position, double radius, double radiantAngle) : base(position, radius)
        {
            this.radiantAngle = radiantAngle;
        }

        public virtual void RotateAnticlockwise(double angle)
        {
            this.radiantAngle = (this.radiantAngle + angle) % (2 * Math.PI);
        }

        //TO_STRING, EQUALS, HASHCODE
        public override string ToString()
        {
            return "RotatingEntity[ID=" + this.ID + ", position=" + this.Position +
                   ", radius=" + this.Radius + ", radiantRotation=" + this.Rotation + "]";
        }
        public override bool Equals(object obj)
        {
            return obj is RotatingEntity entity &&
                   base.Equals(obj) &&
                   Rotation == entity.Rotation;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Rotation);
        }
    }

}
