﻿using System;
using System.Collections;
using System.Collections.Generic;
using ConsoleApp;
using OverGit.entity;

namespace OverGit.entity
{
	/// <summary>
	/// Identifies all the given input by the user.
	/// </summary>
	public enum PlayerAction
	{
		/// <summary>
		/// Identifies the acceleration input of the Playership given by the user.
		/// </summary>
		ACCELERATE,

		/// <summary>
		/// Identifies the clockwise rotation input of the Playership given by the user.
		/// </summary>
		ROTATE_CLOCKWISE,

		/// <summary>
		/// Identifies the deceleration input of the Playership given by the user.
		/// </summary>
		DECELERATE,

		/// <summary>
		/// Identifies the anticlockwise rotation input of the Playership given by the user.
		/// </summary>
		ROTATE_ANTICLOCKWISE,

		/// <summary>
		/// Identifies the shoot input of the Playership given by the user.
		/// </summary>
		SHOOT
	}
	/// <summary>
	/// The controller that manages the player actions.
	/// </summary>
	public class PlayerHandlerImpl
	{

		private readonly IDictionary<PlayerAction, bool> keys = new Dictionary<PlayerAction, bool>();
		private readonly BasicPlayerShipImpl player;

		public PlayerHandlerImpl(BasicPlayerShipImpl player)
		{
			this.player = (BasicPlayerShipImpl) player;
			foreach (PlayerAction action in Enum.GetValues(typeof(PlayerAction)))
			{
				this.keys.Add(action, false);
			}
		}

		/// <summary>
		/// Finds, if presents, the mapped key and set its value to true.
		/// </summary>
		/// <param name="key"> action the action performed by the user </param>
		public virtual void Activate(PlayerAction key)
		{
			this.keys[key] = true;
		}

		/// <summary>
		/// Finds, if presents, the mapped Key and set its value to false.
		/// </summary>
		/// <param name="key"> action the action the user doesn't perform anymore </param>
		public virtual void Deactivate(PlayerAction key)
		{
			this.keys[key] = false;
		}

		/// <summary>
		/// Tells the player how to act according to the keys pressed by the user.
		/// </summary>
		public virtual void ComputeAction()
		{
			foreach(var entry in this.keys)
			{
				if (entry.Value)
				{
					switch (entry.Key)
					{
						case PlayerAction.ACCELERATE:
							Accelerate();
							break;
						case PlayerAction.DECELERATE:
							Decelerate();
							break;
						case PlayerAction.ROTATE_ANTICLOCKWISE:
							RotateAnticlockwise();
							break;
						case PlayerAction.ROTATE_CLOCKWISE:
							RotateClockwise();
							break;
						default:
							break;
					}
				}
			}

			

		}

		/// <summary>
		/// Add the score passed to the actual player score.
		/// </summary>
		/// <param name="scorePoint"> scorePoint the score obtained by the player </param>
		public virtual void AddScore(int scorePoint)
		{
			this.player.Score.AddScorePoints(scorePoint);
		}

		/// <summary>
		/// Increment levels beaten by the player. 
		/// </summary>
		public virtual void BeatLevel()
		{
			this.player.Score.IncrementLevelBeaten();
		}

		/// <summary>
		/// Rotate the player clockwise according to it's maximum angular speed.
		/// </summary>
		private void RotateClockwise()
		{
			this.player.RotateClockwise(this.player.MaxAngularSpeed);
		}

		/// <summary>
		/// Rotate the player anticlockwise according to it's maximum angular speed.
		/// </summary>
		private void RotateAnticlockwise()
		{
			this.player.RotateAnticlockwise(this.player.MaxAngularSpeed);
		}

		/// <summary>
		/// Decelerate the player according to it's maximum acceleration and in the
		/// direction it's facing.
		/// </summary>
		private void Decelerate()
		{
			this.player.Decelerate(new Vector2D(this.player.MaxAcceleration, this.player.Rotation));
		}

		/// <summary>
		/// Accelerate the player according to it's maximum acceleration and in the
		/// direction it's facing.
		/// </summary>
		private void Accelerate()
		{
			this.player.Accelerate(new Vector2D(this.player.MaxAcceleration, this.player.Rotation));
		}

	}

}