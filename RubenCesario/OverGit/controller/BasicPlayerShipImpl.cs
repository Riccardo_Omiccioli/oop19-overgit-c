﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleApp;
using static OverGit.entity.BasicSpaceShipImpl;

namespace OverGit.entity
{
    /// <summary>
    /// Standard implementation for BasicPlayerShip.
    /// </summary>
    public class BasicPlayerShipImpl : BasicSpaceShipImpl
    {
        public BasicPlayerShipImpl(Point2D position, double radiantAngle, Vector2D speed) : base(position, radiantAngle, speed)
        {
            Score = new PlayerScore();
        }

        public BasicPlayerShipImpl(Point2D position, double radiantAngle, Vector2D speed, BasicSpaceShipTemplate model, string playerName) : base(position, radiantAngle, speed, model)
        {
            Score = new PlayerScore(playerName);
        }

        public PlayerScore Score {get; set; }

        /// <summary>
        /// The model of the player score: bounds each score to a name 
        /// of some significance.
        /// </summary>
        [Serializable]
        public class PlayerScore
        {
            /// <summary> Defines the name of a default score in the leaderboard. </summary>
            public static readonly string DEFAULT_NAME = "Overgit_Bot";
            private static readonly long serialversionUID = 4160579278796847063L;
            public string PlayerName { get; set; }

            public int TotalScore { get; set; }

            public int LevelBeaten { get; set; }
            public PlayerScore(string playerName, int totalScore, int levelBeaten)
            {
                this.PlayerName = playerName;
                this.TotalScore = totalScore;
                this.LevelBeaten = levelBeaten;
            }
            public PlayerScore(string playerName) : this(playerName, 0, 0) { }
            public PlayerScore() : this(DEFAULT_NAME, 0, 0) {}

            /// <summary>
            /// Adds the specified score points to the total score points of this score.
            /// </summary>
            /// <param name="scorePoints"> the specified score points. </param>
            public void AddScorePoints(int scorePoints)
            {
                this.TotalScore += scorePoints;
            }

            /// <summary>
            /// Increments by one the number of level beaten by the owner of this score.
            /// </summary>
            public void IncrementLevelBeaten()
            {
                this.LevelBeaten++;
            }
        }
    }
}
