﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleApp;
using static OverGit.entity.BasicSpaceShipImpl;

namespace OverGit.entity
{
	/// <summary> Models a BasicSpaceShip factory that makes building ship more user-friendly. </summary>
	class BasicSpaceShipFactory
    {
		private static readonly IDictionary<EntityID, BasicSpaceShipTemplate> PLAYERMODEL_TABLE = new Dictionary<EntityID, BasicSpaceShipTemplate>();
		private static readonly Vector2D INITIAL_SPEED = new Vector2D(0, 0);
		private const double INITIAL_ROTATION = 0;

		static BasicSpaceShipFactory()
		{
			initializeData();
		}

		/// <summary> Builds a PlayerShip out of the specified model at the given position. </summary>
		/// <param name="modelID"> model of the ship to be built </param>
		/// <param name="position"> position where the ship will be built </param>
		/// <param name="playerName"> name of the player in control of this ship </param>
		/// <returns> Returns a PlayerShip out of the specified model at the given position. </returns>
		public virtual BasicPlayerShipImpl buildPlayerShip(EntityID modelID, Point2D position, string playerName)
		{
			return new BasicPlayerShipImpl(position, INITIAL_ROTATION, INITIAL_SPEED, PLAYERMODEL_TABLE[EntityIDExtension.RequireBelonging(modelID, EntityIDCategory.SPACESHIPS_BASIC)], playerName);
		}

		/// <summary> Initialises the tables that map each EntityID to their model both for enemy ships and for player ships. </summary>
		private static void initializeData()
		{
			PLAYERMODEL_TABLE.Add(EntityID.SPACESHIP_BASIC, new BasicSpaceShipTemplate(EntityID.SPACESHIP_BASIC, 1000, 1.2, 0.05, Math.PI / 50, 2, 0.04));
			PLAYERMODEL_TABLE.Add(EntityID.FIGHTER, new BasicSpaceShipTemplate(EntityID.FIGHTER, 700, 0.95, 0.04, Math.PI / 60, 2, 0.04));
			PLAYERMODEL_TABLE.Add(EntityID.JUGGERNAUT, new BasicSpaceShipTemplate(EntityID.JUGGERNAUT, 1500, 0.55, 0.03, Math.PI / 70, 2, 0.05));
			PLAYERMODEL_TABLE.Add(EntityID.CUTTER, new BasicSpaceShipTemplate(EntityID.CUTTER, 1000, 1.2, 0.05, Math.PI / 50, 2, 0.03));

		}


	}
}
