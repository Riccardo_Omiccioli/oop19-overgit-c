﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleApp;

namespace OverGit.entity
{
    class TestPlayerHandler
    {   
        public static void Main()
        {
            
            Console.Out.WriteLine("-----------------------------------------------------------");
            Console.Out.WriteLine("TEST - PLAYERHANDLER");
            Console.Out.WriteLine("TEST 1/2 - TestIncrementScoreAndLevel");
            Console.Out.WriteLine("-----------------------------------------------------------");
            BasicSpaceShipFactory factory = new BasicSpaceShipFactory();
            BasicPlayerShipImpl player = factory.buildPlayerShip(EntityID.SPACESHIP_BASIC, new Point2D(), "player1");
            PlayerHandlerImpl playerHandler = new PlayerHandlerImpl(player);
            Console.Out.WriteLine("Expected [0] level beaten. Result: ["+ player.Score.LevelBeaten + "].");
            Console.Out.WriteLine("Expected [0] represent the score of the player. Result: [" + player.Score.TotalScore +"].");
            Console.Out.WriteLine("Incrementing the score of the player by 500.");
            playerHandler.AddScore(500);
            Console.Out.WriteLine("Expected [500] represent the score of the player. Result: [" + player.Score.TotalScore + "].");
            Console.Out.WriteLine("Incrementing the score of the player by 2000.");
            playerHandler.AddScore(2000);
            Console.Out.WriteLine("Expected [2500] represent the score of the player. Result: [" + player.Score.TotalScore + "].");
            Console.Out.WriteLine("Forcing player to beat 3 level.");
            playerHandler.BeatLevel();
            playerHandler.BeatLevel();
            playerHandler.BeatLevel();
            Console.Out.WriteLine("Expected [3] level beaten. Result: [" + player.Score.LevelBeaten + "].");
            Console.Out.WriteLine("-----------------------------------------------------------");
            Console.Out.WriteLine("TEST 2/2 - TestAction");
            Console.Out.WriteLine("-----------------------------------------------------------");
            Console.Out.WriteLine("Test that call Activate() method with an Action don't change any data of the player.");
            Console.Out.WriteLine("The action tested is ROTATE_CLOCKWISE");
            playerHandler.Activate(PlayerAction.ROTATE_CLOCKWISE);
            Console.Out.WriteLine("Expected [0.0] rotation of the player. Result: [" + player.Rotation + "].");
            Console.Out.WriteLine("Add ROTATE_ANTICLOCKWISE to the active actions");
            playerHandler.Activate(PlayerAction.ROTATE_ANTICLOCKWISE);
            Console.Out.WriteLine("Call ComputeAction() multiple times to see that since ROTATE_CLOCKWISE and ROTATE_ANTICLOCKWISE are active the overall rotation musn't change.");
            playerHandler.ComputeAction();
            playerHandler.ComputeAction();
            playerHandler.ComputeAction();
            Console.Out.WriteLine("Expected [0.0] rotation of the player. Result: [" + player.Rotation + "].");
            Console.Out.WriteLine("Remove ROTATE_CLOCKWISE from the active actions");
            playerHandler.Deactivate(PlayerAction.ROTATE_CLOCKWISE);
            Console.Out.WriteLine("Call ComputeAction(). Now, since only ROTATE_ANTICLOCKWISE is active, the overall rotation must change");
            playerHandler.ComputeAction();
            Console.Out.WriteLine("Expected NOT [0.0] rotation of the player. Result: [" + player.Rotation + "].");
            playerHandler.Deactivate(PlayerAction.ROTATE_ANTICLOCKWISE);
            Console.Out.WriteLine("Add ACCELERATE to the active actions and call ComputeAction(). The player speed must change.");
            playerHandler.Activate(PlayerAction.ACCELERATE);
            playerHandler.ComputeAction();
            Console.Out.WriteLine("Expected NOT [" + new Vector2D() + "] player speed. Result: [" + player.Speed + "].");
            Console.Out.WriteLine("-----------------------------------------------------------");
            Console.Out.WriteLine("End of Test");
            Console.Out.WriteLine("-----------------------------------------------------------");
            Console.Out.WriteLine("Press any key to exit the test.");
            Console.ReadLine();
            
        }
        
    }
}
