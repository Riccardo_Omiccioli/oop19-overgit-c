﻿using System.Collections.Generic;
using ConsoleApp;

namespace OverGit.entity
{
	/// <summary>
	/// Standard implementation for BasicSpaceShip.
	/// </summary>
	public class BasicSpaceShipImpl : MovingEntity
	{

		private static readonly BasicSpaceShipTemplate DEFAULT_MODEL = new BasicSpaceShipTemplate(EntityID.SPACESHIP_BASIC, 0, 0, 0, 0, 0, 0D);
		private const double MOVEMENT_TOLLERANCE = 0.001;
		private readonly BasicSpaceShipTemplate shipModel;
		private double health;

		public BasicSpaceShipImpl(Point2D position, double radiantAngle, Vector2D speed) : this(position, radiantAngle, speed, DEFAULT_MODEL) { }
		public BasicSpaceShipImpl(Point2D position, double radiantAngle, Vector2D speed, BasicSpaceShipTemplate model) : base(position, model.radius, radiantAngle, speed)
		{
			this.shipModel = model;
			this.health = this.shipModel.maxHealth;
		}

		/// <inheritdoc/>
		public override EntityID ID
		{
			get	{ return this.shipModel.modelID; }
		}

		/// <inheritdoc/>
		public override void RotateAnticlockwise(double angle)
		{
			if (angle > this.MaxAngularSpeed + MOVEMENT_TOLLERANCE)
			{
				throw new System.ArgumentException("The ship [" + this.ID + "] cannot rotate this fast.");
			}
			base.RotateAnticlockwise(angle);
		}

		/// <inheritdoc/>
		public override void Move()
		{
			base.Move();
			this.DecelerateByDrag();
		}

		/// <inheritdoc/>
		public override void Accelerate(Vector2D acceleration)
		{
			base.Accelerate(acceleration);
			this.CapSpeed();
		}

		/// <summary>
		/// For a BasicSpaceShip, reduces its health by the amount of damage received.
		/// </summary>
		/// <param name="damage"> the damage received </param>
		public virtual void ReceiveDamage(double damage)
		{
			this.health -= damage;
		}

		public virtual int MaxHealth
		{
			get	{ return this.shipModel.maxHealth; }
		}

		public virtual double Health
		{
			get	{ return health; }
		}

		public virtual double MaxSpeed
		{
			get	{ return this.shipModel.maxSpeed; }
		}

		public virtual double MaxAcceleration
		{
			get	{ return this.shipModel.maxAcceleration; }
		}

		public virtual double MaxAngularSpeed
		{
			get	{ return this.shipModel.maxAngularSpeed; }
		}

		public virtual double Drag
		{
			get	{ return this.shipModel.drag; }
		}

		/*SETTERS & GETTERS------------------------------------------------*/
		public virtual BasicSpaceShipTemplate ShipModel
		{
			get	{ return shipModel; }
		}
		/*-----------------------------------------------------------------*/

		/// <summary>
		/// Reduces the speed of this ship to a certain percentage depending
		/// on its drag.The higher the drag, the lower the percentage.
		/// </summary>
		protected void DecelerateByDrag()
		{
			this.ResetSpeed(this.Speed.MultiplyByScalar(1 - this.Drag));
			this.CapSpeed();
		}

		/// <summary>
		/// Caps the speed of this ship to its maximum speed or zero, assuming 
		/// BasicSpaceShip can't accelerate over its maximum speed or decelerate
		/// till moving backwards.
		/// </summary>
		private void CapSpeed()
		{
			if (this.Speed.Module > this.MaxSpeed)
			{
				this.ResetSpeed(this.Speed.MultiplyByScalar(this.MaxSpeed / this.Speed.Module));
			}
			else if (this.Speed.Module < 0)
			{
				this.ResetSpeed(new Vector2D(0, 0));
			}
		}

		/// <summary>
		/// Represents the model of a BasicSpaceShip, containing all its
		/// model-dependent properties.
		/// </summary>
		public class BasicSpaceShipTemplate
		{

			internal readonly EntityID modelID;
			internal readonly int maxHealth;
			internal readonly double maxSpeed;
			internal readonly double maxAcceleration;
			internal readonly double maxAngularSpeed;
			internal readonly double radius;
			internal readonly double drag;

			public BasicSpaceShipTemplate(EntityID modelID, int maxHealth, double maxSpeed, double maxAcceleration, double maxAngularSpeed, double radius, double drag)
			{
				this.modelID = EntityIDExtension.RequireBelonging(modelID, EntityIDCategory.SPACESHIPS_BASIC);
				this.maxHealth = maxHealth;
				this.maxSpeed = maxSpeed;
				this.maxAcceleration = maxAcceleration;
				this.maxAngularSpeed = maxAngularSpeed;
				this.drag = drag;
				this.radius = radius;
			}

			public virtual EntityID ModelID
			{
				get	{ return this.modelID; }
			}
		}

	}
}
