﻿/* 
 * Translation by Jahrim Gabriele Cesario 
 */
using System;

namespace ConsoleApp
{
    public class Point2D
    {
        //FIELDS
        private double x;
        private double y;

        //PROPERTIES
        public static Point2D Origin => new Point2D(0, 0);
        public double X
        {
            get { return x; }
            set { x = value; }
        }
        public double Y
        {
            get { return y; }
            set { y = value; }
        }

        //METHODS
        public Point2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
        public Point2D() : this(0, 0) { }

        /**
         * Saves a copy of the specified source point in the specified destination point.
         */
        public static void Copy(Point2D src, out Point2D dest) => dest = new Point2D(src.X, src.Y);

        /**
         * Translates this point of the specified translation.
         */
        public Point2D Translate(Point2D t)
        {
            return new Point2D(this.x + t.x, this.y + t.y);
        }
        /**
         * Returns the distance between this point and the specified point.
         */
        public double Distance(Point2D p)
        {
            return Math.Sqrt((this.x - p.x) * (this.x - p.x) + (this.y - p.y) * (this.y - p.y));
        }

        //TO_STRING, EQUALS, HASHCODE
        public override string ToString()
        {
            return "(" + this.x + "," + this.y + ")";
        }
        public override bool Equals(object obj)
        {
            return obj is Point2D d &&
                   x == d.x &&
                   y == d.y;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(x, y);
        }
    }
}
