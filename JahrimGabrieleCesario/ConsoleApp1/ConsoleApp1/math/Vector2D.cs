﻿/* 
 * Translation by Jahrim Gabriele Cesario 
 */
using System;

namespace ConsoleApp
{
    public class Vector2D
    {
        //FIELDS
        private double x;
        private double y;

        //PROPERTIES
        public double X
        {
            get { return x; }
            set { x = value; }
        }
        public double Y
        {
            get { return y; }
            set { y = value; }
        }
        public Point2D Components => new Point2D(x, y);
        public double Module => this.Components.Distance(Point2D.Origin);
        public double Angle
        {
            get {
                if (this.x == 0)
                    return (this.y == 0) ? 0 :
                           (this.y > 0 ) ? Math.PI/2 : 3/2*Math.PI;
                double atan = Math.Atan(this.y / this.x);
                return (atan + ((this.x < 0) ? Math.PI : 2*Math.PI)) % (2*Math.PI);
            }
        }

        //METHODS
        public Vector2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
        public Vector2D(Point2D components) : this(components.X, components.Y) { }
        public Vector2D() : this(0, 0) { }

        /**
         * Saves a copy of the specified source vector in the specified destination vector.
         */
        public static void Copy(Vector2D src, out Vector2D dest) => dest = new Vector2D(src.Components);
        /**
         * Returns the distance vector between the specified points.
         */
        public static Vector2D DistanceVector(Point2D p1, Point2D p2) => new Vector2D(p2.X - p1.X, p2.Y - p1.Y);
        /**
         * Returns the vector with the specified module and direction.
         */
        public static Vector2D BuildFromModule(double module, double angle)
        {
            return new Vector2D(module * Math.Cos(angle), module * Math.Sin(angle));
        }

        /**
         * Returns the opposite of this vector.
         */
        public Vector2D Opposite()
        {
            return new Vector2D(-this.x, -this.y);
        }
        /**
         * Returns the sum between this vector and the specified vector.
         */
        public Vector2D Add(Vector2D v)
        {
            return new Vector2D(this.Components.Translate(v.Components));
        }
        /**
         * Returns the difference between this vector and the specified vector.
         */
        public Vector2D Sub(Vector2D v) => Add(v.Opposite());
        /**
         * Returns the product between this vector and the specified scalar value.
         */
        public Vector2D MultiplyByScalar(double scalar)
        {
            return new Vector2D(this.x * scalar, this.y * scalar);
        }
        /**
         * Returns the product between this vector and the specified vector.
         */
        public double MultiplyByVector(Vector2D v)
        {
            return this.x * v.x + this.y * v.y;
        }
        /**
         * Returns the angle between this vector and the specified vector.
         */
        public double AngleBetween(Vector2D v)
        {
            return Math.Acos(this.MultiplyByVector(v) / (this.Module * v.Module));
        }

        //TO_STRING, EQUALS, HASHCODE
        public override string ToString()
        {
            return "V(" + this.x + "," + this.y + ")";
        }
        public override bool Equals(object obj)
        {
            return obj is Vector2D d &&
                   x == d.x &&
                   y == d.y;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(x, y);
        }

        //OPERATORS
        public static Vector2D operator +(Vector2D v) => v;
        public static Vector2D operator +(Vector2D v1, Vector2D v2) => v1.Add(v2);
        public static Vector2D operator -(Vector2D v) => v.Opposite();
        public static Vector2D operator -(Vector2D v1, Vector2D v2) => v1.Add(-v2);
        public static Vector2D operator *(Vector2D v, double s) => v.MultiplyByScalar(s);
        public static double operator *(Vector2D v1, Vector2D v2) => v1.MultiplyByVector(v2);
    }
}
