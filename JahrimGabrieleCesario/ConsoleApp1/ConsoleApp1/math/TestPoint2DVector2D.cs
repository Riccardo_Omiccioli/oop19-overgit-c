﻿/* 
 * Produced by Jahrim Gabriele Cesario 
 */
using System;

namespace ConsoleApp
{
    class TestPoint2DVector2D
    {

        //public static void Main()
        //{
        //    Console.Out.WriteLine("-----------------------------------------------------------");

        //    Console.Out.WriteLine("TEST - POINT2D");
        //    Point2D p1 = new Point2D(2, 2);
        //    Point2D p2 = new Point2D();
        //    Point2D p3;
        //    Console.Out.WriteLine("P1: " + p1);
        //    Console.Out.WriteLine("P2: " + p2);
        //    Point2D.Copy(p1, out p3);
        //    Console.Out.WriteLine("P3 (copy of P1): " + p3);

        //    Console.Out.WriteLine("\nP1.x: " + p1.X + "\t\tP1.y: " + p1.Y);
        //    Console.Out.Write("Setting P3 to (5,5)..."); p3.X = 5; p3.Y = 5;
        //    Console.Out.WriteLine("\t\tP3.x: " + p3.X + "\t\tP3.y: " + p3.Y);

        //    Console.Out.WriteLine("Translating P1" + p1 + " by (+5,+5): " + p1.Translate(new Point2D(5, 5)));
        //    Console.Out.WriteLine("Distance P1-P3: " + p1.Distance(p3));
        //    Console.Out.WriteLine("\nP2 is equal to (0,0)?: " + p2.Equals(new Point2D(0, 0)));
        //    Console.Out.WriteLine("-----------------------------------------------------------");

        //    Console.Out.WriteLine("TEST - VECTOR2D");
        //    Vector2D v1 = new Vector2D(0, 3);
        //    Vector2D v2 = new Vector2D(new Point2D(6, 0));
        //    Vector2D v3 = new Vector2D();
        //    Vector2D v4 = Vector2D.BuildFromModule(10, Math.PI / 3);
        //    Vector2D v5;
        //    Console.Out.WriteLine("V1: " + v1);
        //    Console.Out.WriteLine("V2: " + v2);
        //    Console.Out.WriteLine("V3: " + v3);
        //    Console.Out.WriteLine("V4: " + v4);
        //    Vector2D.Copy(v1, out v5);
        //    Console.Out.WriteLine("V5 (copy of V1): " + v5);

        //    Console.Out.WriteLine("\nV1.x: " + v1.X + "\t\tV1.y: " + v1.Y);
        //    Console.Out.Write("Setting V5 to (5,5)..."); v5.X = 5; v5.Y = 5;
        //    Console.Out.WriteLine("\t\tV5.x: " + v5.X + "\t\tV5.y: " + v5.Y);

        //    Console.Out.WriteLine("\nV1.module: " + v1.Module);
        //    Console.Out.WriteLine("V1.angle: " + ToDegrees(v1.Angle));
        //    Console.Out.WriteLine("V1.components: " + v1.Components);
        //    Console.Out.WriteLine("V2.module: " + v2.Module);
        //    Console.Out.WriteLine("V2.angle: " + ToDegrees(v2.Angle));
        //    Console.Out.WriteLine("V2.components: " + v2.Components);

        //    Console.Out.WriteLine("\nDistanceVector P1" + p1 + "->P3" + p3 + ": " + Vector2D.DistanceVector(p1, p3));
        //    Console.Out.WriteLine("Opposite of V1: " + v1.Opposite() + "\t\t(" + (-v1) + ")");
        //    Console.Out.WriteLine("Sum of V1+V2: " + v1.Add(v2) + "\t\t(" + (v1 + v2) + ")");
        //    Console.Out.WriteLine("Difference between V5-V1: " + v5.Sub(v1) + "\t\t(" + (v5 - v1) + ")");
        //    Console.Out.WriteLine("\nScalar Product V5*10: " + v5.MultiplyByScalar(10) + "\t\t(" + (v5 * 10) + ")");
        //    Console.Out.WriteLine("Cross Product V1xV2: " + v1.MultiplyByVector(v2) + "\t\t(" + (v1 * v2) + ")");
        //    Console.Out.WriteLine("Angle between V1&V2: " + ToDegrees(v1.AngleBetween(v2)));
        //    Console.Out.WriteLine("Cross Product V1xV5: " + v1.MultiplyByVector(v5) + "\t\t(" + (v1 * v5) + ")");
        //    Console.Out.WriteLine("Angle between V1&V5: " + ToDegrees(v1.AngleBetween(v5)));
        //    Console.Out.WriteLine("\nV3 is equal to (0,0)?: " + v3.Equals(new Vector2D(0, 0)));
        //    Console.Out.WriteLine("-----------------------------------------------------------");

        //}

        private static double ToDegrees(double radiantAngle)
        {
            return radiantAngle / (2 * Math.PI) * 360;
        }
    }
}
