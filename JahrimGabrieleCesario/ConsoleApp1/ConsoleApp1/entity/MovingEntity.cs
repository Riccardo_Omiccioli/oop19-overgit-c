﻿/* 
 * Translation by Jahrim Gabriele Cesario 
 */
using System;
using System.Collections.Generic;

namespace ConsoleApp
{

    public interface IMovingEntity : IRotatingEntity
    {
        //PROPERTIES
        Vector2D Speed { get; set; }

        //METHODS
        /**
          * Changes the position of this entity according to its speed and
          * possibly other factors.
          */
        void Move();
        /**
          * Increases the speed of this entity by a certain amount towards the
          * direction of its acceleration.
          */
        void Accelerate(Vector2D acceleration);
    }
    //DEFAULT METHODS
    public static class IMovingEntityHelper
    {
        /**
          * Decreases the speed of this entity by a certain amount towards the
          * direction of its acceleration.
          */
        public static void Decelerate(this IMovingEntity entity, Vector2D acceleration) => entity.Accelerate(acceleration.Opposite());
    }

    public class MovingEntity : RotatingEntity, IMovingEntity
    {
        //FIELDS
        private static readonly Vector2D DEFAULT_SPEED = new Vector2D(new Point2D(0, 0));

        private Vector2D speed;

        //PROPERTIES
        public Vector2D Speed
        {
            get { return this.speed; }
            set { this.speed = value; }
        }

        //METHODS
        public MovingEntity() : base()
        {
            this.speed = MovingEntity.DEFAULT_SPEED;
        }
        public MovingEntity(Point2D position, double radius, double radiantAngle) 
            : this(position, radius, radiantAngle, MovingEntity.DEFAULT_SPEED) { }
        public MovingEntity(Point2D position, double radius, double radiantAngle, Vector2D speed) 
            : base(position, radius, radiantAngle)
        {
            this.speed = speed;
        }

        public virtual void Move()
        {
            this.Position = this.Position.Translate(this.speed.Components);
        }

        public virtual void Accelerate(Vector2D acceleration)
        {
            this.speed = this.speed.Add(acceleration);
        }

        //TO_STRING, EQUALS, HASHCODE
        public override string ToString()
        {
            return "MovingEntity[ID=" + this.ID + ", position=" + this.Position +
                   ", radius=" + this.Radius + ", radiantRotation=" + this.Rotation +
                   ", speed=" + this.Speed + "]";
        }
        public override bool Equals(object obj)
        {
            return obj is MovingEntity entity &&
                   base.Equals(obj) &&
                   EqualityComparer<Vector2D>.Default.Equals(Speed, entity.Speed);
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Speed);
        }
    }

}
