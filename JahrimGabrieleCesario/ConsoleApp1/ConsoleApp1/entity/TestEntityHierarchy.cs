﻿/* 
 * Produced by Jahrim Gabriele Cesario 
 */
using System;

namespace ConsoleApp
{
    class TestEntityHierarchy
    {

        //public static void Main()
        //{
        //    Console.Out.WriteLine("-----------------------------------------------------------");

        //    Console.Out.WriteLine("TEST - ENTITY");
        //    Point2D p1 = new Point2D(0, 0);
        //    Point2D p2 = new Point2D(0, 3);

        //    IEntity e1 = new Entity(p1);
        //    IEntity e2 = new Entity(p2);
        //    IEntity e3 = new Entity(p1);
        //    Console.Out.WriteLine("E1: " + e1);
        //    Console.Out.WriteLine("E2: " + e2);
        //    Console.Out.WriteLine("E3: " + e3);

        //    Console.Out.WriteLine("\nE1 is equal to E2?: " + e1.Equals(e2));
        //    Console.Out.WriteLine("E1 is equal to E3?: " + e1.Equals(e3));

        //    Console.Out.WriteLine("\nE1 is in position P1" + p1 + "?: " + e1.Position.Equals(p1));
        //    Console.Out.WriteLine("E3 is in position P2" + p2 + "?: " + e3.Position.Equals(p2));

        //    Console.Out.WriteLine("\nResetting E3 in position P2...");
        //    e3.Position = p2;
        //    Console.Out.WriteLine("E3 is in position P2" + p2 + "?: " + e3.Position.Equals(p2));
        //    Console.Out.WriteLine("-----------------------------------------------------------");

        //    Console.Out.WriteLine("TEST - COLLIDABLE_ENTITY");

        //    ICollidableEntity ce1 = new CollidableEntity(p1, 3);
        //    ICollidableEntity ce2 = new CollidableEntity(p2, 2);
        //    ICollidableEntity ce3 = new CollidableEntity(p1, 3);
        //    ICollidableEntity ce4 = new CollidableEntity(p1, 4);
        //    Console.Out.WriteLine("CE1: " + ce1);
        //    Console.Out.WriteLine("CE2: " + ce2);
        //    Console.Out.WriteLine("CE3: " + ce3);
        //    Console.Out.WriteLine("CE4: " + ce4);

        //    Console.Out.WriteLine("\nCE1 is equal to CE2?: " + ce1.Equals(ce2));
        //    Console.Out.WriteLine("CE1 is equal to CE3?: " + ce1.Equals(ce3));
        //    Console.Out.WriteLine("CE1 is equal to CE4?: " + ce1.Equals(ce4));

        //    Console.Out.WriteLine("\nCE1 has a radius of 3?: " + ce1.Radius.Equals(3));
        //    Console.Out.WriteLine("CE2 has a radius of 2?: " + ce2.Radius.Equals(2));
        //    Console.Out.WriteLine("CE3 has a radius of 1.5?: " + ce3.Radius.Equals(1.5));
        //    Console.Out.WriteLine("CE4 has a radius of 4?: " + ce4.Radius.Equals(4));
        //    Console.Out.WriteLine("-----------------------------------------------------------");

        //    Console.Out.WriteLine("TEST - ROTATING_ENTITY");

        //    IRotatingEntity re1 = new RotatingEntity(p1, 3, 0);
        //    IRotatingEntity re2 = new RotatingEntity(p2, 2, Math.PI / 2);
        //    IRotatingEntity re3 = new RotatingEntity(p1, 3, 0);
        //    Console.Out.WriteLine("RE1: " + re1);
        //    Console.Out.WriteLine("RE2: " + re2);
        //    Console.Out.WriteLine("RE3: " + re3);

        //    Console.Out.WriteLine("\nRE1 is equal to RE2?: " + re1.Equals(re2));
        //    Console.Out.WriteLine("RE1 is equal to RE3?: " + re1.Equals(re3));

        //    Console.Out.WriteLine("\nRE1 is facing right?: " + re1.Rotation.Equals(0));
        //    Console.Out.WriteLine("RE2 is facing up?: " + re2.Rotation.Equals(Math.PI / 2));
        //    Console.Out.WriteLine("RE3 is facing up?: " + re3.Rotation.Equals(Math.PI / 2));

        //    Console.Out.WriteLine("\nResetting RE3 rotation to face up...");
        //    re3.Rotation = Math.PI / 2;
        //    Console.Out.WriteLine("RE3 is facing up?: " + re3.Rotation.Equals(Math.PI / 2));

        //    Console.Out.WriteLine("\nRotating RE1 of PI/2 anticlockwise...");
        //    re1.RotateAnticlockwise(Math.PI / 2);
        //    Console.Out.WriteLine("RE1 is facing up?: " + re1.Rotation.Equals(Math.PI / 2));
        //    Console.Out.WriteLine("Rotating RE1 of 4*PI anticlockwise (redundant rotation)...");
        //    re1.RotateAnticlockwise(4 * Math.PI);
        //    Console.Out.WriteLine("RE1 is facing up?: " + re1.Rotation.Equals(Math.PI / 2));

        //    Console.Out.WriteLine("\nRotating RE3 of PI clockwise...");
        //    re3.RotateClockwise(Math.PI);
        //    Console.Out.WriteLine("RE3 is facing down?: " + re3.Rotation.Equals(-Math.PI / 2));
        //    Console.Out.WriteLine("Rotating RE3 of 4*PI clockwise (redundant rotation)...");
        //    re3.RotateClockwise(4 * Math.PI);
        //    Console.Out.WriteLine("RE3 is facing up?: " + re3.Rotation.Equals(-Math.PI / 2));
        //    Console.Out.WriteLine("-----------------------------------------------------------");

        //    Console.Out.WriteLine("TEST - MOVING_ENTITY");
        //    Vector2D speed1 = new Vector2D(new Point2D(0, 2));
        //    Vector2D speed2 = new Vector2D(new Point2D(2, 0));
        //    IMovingEntity me1 = new MovingEntity(p1, 3, 0, speed1);
        //    IMovingEntity me2 = new MovingEntity(p2, 2, Math.PI / 2, speed2);
        //    IMovingEntity me3 = new MovingEntity(p1, 3, 0, speed1);
        //    Console.Out.WriteLine("ME1: " + me1);
        //    Console.Out.WriteLine("ME2: " + me2);
        //    Console.Out.WriteLine("ME3: " + me3);

        //    Console.Out.WriteLine("\nME1 is equal to ME2?: " + me1.Equals(me2));
        //    Console.Out.WriteLine("ME1 is equal to ME3?: " + me1.Equals(me3));

        //    Console.Out.WriteLine("\nME1 has a speed of " + speed1 + "?: " + me1.Speed.Equals(speed1));
        //    Console.Out.WriteLine("ME2 has a speed of " + speed2 + "?: " + me2.Speed.Equals(speed2));
        //    Console.Out.WriteLine("ME3 has a speed of " + speed2 + "?: " + me3.Speed.Equals(speed2));

        //    Console.Out.WriteLine("\nResetting ME3 speed to " + speed2 + "...");
        //    me3.Speed = speed2;
        //    Console.Out.WriteLine("ME3 has a speed of " + speed2 + "?: " + me3.Speed.Equals(speed2));

        //    Vector2D acceleration = new Vector2D(new Point2D(10, 10));

        //    Vector2D expectedVelocityM1 = new Vector2D(new Point2D(10, 12));
        //    Console.Out.WriteLine("\nAccelerating ME1 of " + acceleration + "...");
        //    me1.Accelerate(acceleration);
        //    Console.Out.WriteLine("ME1 has a speed of " + expectedVelocityM1 + "?: " + me1.Speed.Equals(expectedVelocityM1));

        //    Vector2D expectedVelocityM3 = new Vector2D(new Point2D(-8, -10));
        //    Console.Out.WriteLine("\nDecelerating ME3 of " + acceleration + "...");
        //    me3.Decelerate(acceleration);
        //    Console.Out.WriteLine("ME3 has a speed of " + expectedVelocityM3 + "?: " + me3.Speed.Equals(expectedVelocityM3));
        //    Console.Out.WriteLine("-----------------------------------------------------------");

        //    Console.Out.WriteLine("TEST - CONDITIONAL_ENTITY");
        //    IConditionalEntity conE1 = new ConditionalEntity(() => true);
        //    IConditionalEntity conE2 = new ConditionalEntity(() => 3 < 2);

        //    Console.Out.WriteLine("ConE1: <I always exist>");
        //    Console.Out.WriteLine("ConE2: <I exist whenever [3 < 2] is true>");

        //    Console.Out.WriteLine("\nConE1 is existing?: " + conE1.IsExisting());
        //    Console.Out.WriteLine("ConE1 is not existing?: " + conE1.IsntExisting());

        //    Console.Out.WriteLine("\nConE2 is existing?: " + conE2.IsExisting());
        //    Console.Out.WriteLine("ConE2 is not existing?: " + conE2.IsntExisting());
        //    Console.Out.WriteLine("-----------------------------------------------------------");
        //}

    }
}
