﻿/* 
 * Translation by Jahrim Gabriele Cesario 
 */
using System;
using System.Collections.Generic;

namespace ConsoleApp
{

    public interface IEntity
    {
        //PROPERTIES
        EntityID ID { get; }
        Point2D Position { get; set; }
    }

    public class Entity : IEntity
    {
        //FIELDS
        private static readonly Point2D DEFAULT_POSITION = Point2D.Origin;

        private Point2D position;

        //PROPERTIES
        public virtual EntityID ID => EntityID.DEFAULT_ENTITY;
        public Point2D Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        //METHODS
        public Entity() : this(Entity.DEFAULT_POSITION) { }
        public Entity(Point2D position)
        {
            this.position = position;
        }

        //TO_STRING, EQUALS, HASHCODE
        public override string ToString()
        {
            return "Entity [ID=" + this.ID + ", position=" + this.Position + "]";
        }
        public override bool Equals(object obj)
        {
            return obj is Entity entity &&
                   ID == entity.ID &&
                   EqualityComparer<Point2D>.Default.Equals(Position, entity.Position);
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(ID, Position);
        }

    }

}
