﻿/* 
 * Translation by Jahrim Gabriele Cesario 
 */
using System;

namespace ConsoleApp
{

    public interface ICollidableEntity : IEntity
    {
        //PROPERTIES
        double Radius { get; }
    }

    public class CollidableEntity : Entity, ICollidableEntity
    {
        //FIELDS
        private static readonly double DEFAULT_RADIUS = 0;

        private double radius;

        //PROPERTIES
        public double Radius
        {
            get { return this.radius; }
            set { this.radius = value; }
        }

        //METHODS
        public CollidableEntity() : base()
        {
            this.radius = CollidableEntity.DEFAULT_RADIUS;
        }
        public CollidableEntity(Point2D position) : this(position, CollidableEntity.DEFAULT_RADIUS) { }
        public CollidableEntity(Point2D position, double radius) : base(position)
        {
            this.radius = radius;
        }

        //TO_STRING, EQUALS, HASHCODE
        public override string ToString()
        {
            return "CollidableEntity[ID=" + this.ID + ", position=" + this.Position +
                   ", radius=" + this.Radius + "]";
        }
        public override bool Equals(object obj)
        {
            return obj is CollidableEntity entity &&
                   base.Equals(obj) &&
                   Radius == entity.Radius;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Radius);
        }
    }

}
