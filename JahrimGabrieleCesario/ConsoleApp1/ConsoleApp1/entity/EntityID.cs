﻿/* 
 * Translation by Jahrim Gabriele Cesario 
 */
using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace ConsoleApp
{

    public enum EntityID
    {
        DEFAULT_ENTITY,

        SPACESHIP_BASIC,

        FIGHTER,

        JUGGERNAUT,

        CUTTER,

        PROJECTILE_STANDARD
    }

    public static class EntityIDExtension
    {
        /**
         * Returns true if this entity belongs to the specified category.
         */
        public static Boolean BelongsTo(this EntityID entity, EntityIDCategory category)
        {
            return category.GetEntityIDs().Contains(entity);
        }

        /**
         * Throw an IllegalArgumentException if the specified EntityID doesn't belong to the specified category.
         */
        public static EntityID RequireBelonging(EntityID entity, EntityIDCategory category)
        {
            if (entity.BelongsTo(category))
                return entity;
            else
                throw new ArgumentException("The EntityID [" + entity + "] doesn't belong to the EntityIDCategory [" + category + "].");
        }
    }

    public enum EntityIDCategory
    {
            SPACESHIPS,

            SPACESHIPS_BASIC,
            
            PROJECTILES
    }

    public static class EntityIDCategoryExtension
    {
        private static readonly ISet<EntityID> SPACESHIP_IDS = new HashSet<EntityID>();
        private static readonly ISet<EntityID> SPACESHIP_BASIC_IDS = new HashSet<EntityID>();
        private static readonly ISet<EntityID> PROJECTILE_IDS = new HashSet<EntityID>();

        static EntityIDCategoryExtension()
        {
            SPACESHIP_BASIC_IDS.UnionWith(new HashSet<EntityID>()
                {
                    EntityID.SPACESHIP_BASIC,
                    EntityID.FIGHTER,
                    EntityID.JUGGERNAUT,
                    EntityID.CUTTER
                }
            );
            SPACESHIP_IDS.UnionWith(SPACESHIP_BASIC_IDS);
            PROJECTILE_IDS.UnionWith(new HashSet<EntityID>()
                {
                    EntityID.PROJECTILE_STANDARD
                }
            );
        }

        /**
         * Returns an unmodifiable set of the EntityIDs belonging to the specified category.
         */
        public static ISet<EntityID> GetEntityIDs(this EntityIDCategory category)
        {
            switch (category)
            {
                case EntityIDCategory.SPACESHIPS:
                    return SPACESHIP_IDS.ToImmutableHashSet<EntityID>();
                case EntityIDCategory.SPACESHIPS_BASIC:
                    return SPACESHIP_BASIC_IDS.ToImmutableHashSet<EntityID>();
                case EntityIDCategory.PROJECTILES:
                    return PROJECTILE_IDS.ToImmutableHashSet<EntityID>();
                default:
                    throw new NotImplementedException("The category [" + category + "] isn't yet considered in this method.");
            }
        }

    }


}
