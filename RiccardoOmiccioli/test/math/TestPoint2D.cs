﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OOP19_OverGit.utilities.math;

namespace OOP19_OverGit.riccardoOmiccioli.test.math
{
    static class TestPoint2D
    {
        public static void Test()
        {
            Console.Write("Creating a new Point2D with default coordinates  |  ");
            var pointA = new Point2D();
            Assert.IsTrue(pointA.X == 0 && pointA.Y == 0, "Exception in test create Point2D");
            Console.WriteLine(pointA + "  |  {0}", (pointA.X == 0 && pointA.Y == 0) ? "TEST OK" : "ERROR");

            Console.Write("Creating a new Point2D with coordinates [1 ; 1]  |  ");
            var pointB = new Point2D(1, 1);
            Assert.IsTrue(pointB.X == 1 && pointB.Y == 1, "Exception in test create Point2D");
            Console.WriteLine(pointB + "  |  {0}", (pointB.X == 1 && pointB.Y == 1) ? "TEST OK" : "ERROR");

            Console.Write("Setting Point2D coordinates to [5 ; 10]          |  ");
            pointA.X = 5;
            pointA.Y = 10;
            Assert.IsTrue(pointA.X == 5 && pointA.Y == 10, "Exception in set Point2D coordinates");
            Console.WriteLine(pointA + "  |  {0}", (pointA.X == 5 && pointA.Y == 10) ? "TEST OK" : "ERROR");

            Console.Write("Testing operator+  |  Correct result [  6 ; 11]  |  Actual result: ");
            Assert.IsTrue((pointA + pointB).X == 6 && (pointA + pointB).Y == 11, "Exception in Point2D operator+");
            Console.WriteLine(pointA + pointB + "  |  {0}", ((pointA + pointB).X == 6 && (pointA + pointB).Y == 11) ? "TEST OK" : "ERROR");

            Console.Write("Testing operator-  |  Correct result [  4 ;  9]  |  Actual result: ");
            Assert.IsTrue((pointA - pointB).X == 4 && (pointA - pointB).Y == 9, "Exception in Point2D operator-");
            Console.WriteLine(pointA - pointB + "  |  {0}", ((pointA - pointB).X == 4 && (pointA - pointB).Y == 9) ? "TEST OK" : "ERROR");

            Console.Write("Testing operator*  |  Correct result [ 10 ; 20]  |  Actual result: ");
            Assert.IsTrue((pointA * 2).X == 10 && (pointA * 2).Y == 20, "Exception in Point2D operator*");
            Console.WriteLine(pointA * 2 + "  |  {0}", ((pointA * 2).X == 10 && (pointA * 2).Y == 20) ? "TEST OK" : "ERROR");

            Console.Write("Testing operator/  |  Correct result [2.5 ;  5]  |  Actual result: ");
            Assert.IsTrue((pointA / 2).X == 2.5 && (pointA / 2).Y == 5, "Exception in Point2D operator/");
            Console.WriteLine(pointA / 2 + "  |  {0}", ((pointA / 2).X == 2.5 && (pointA / 2).Y == 5) ? "TEST OK" : "ERROR");

            Console.Write("Testing Point2D distance   |  Correct result ~= 1,41  |  Actual result: ");
            pointA.X = 1;
            pointA.Y = 1;
            pointB.X = 2;
            pointB.Y = 2;
            Assert.IsTrue(pointA.Distance(pointB) == Math.Sqrt(2), "Exception in Point2D Distance");
            Console.WriteLine(string.Format("{0, 6:f4}", pointA.Distance(pointB)) + "  |  {0}", pointA.Distance(pointB) == Math.Sqrt(2) ? "TEST OK" : "ERROR");

            Console.Write("Testing Point2D translate  |  Correct result [3 ; 3]  |  Actual result: ");
            Assert.IsTrue(pointA.Translate(pointB).X == 3 && pointA.Translate(pointB).Y == 3, "Exception in Point2D Translate");
            Console.WriteLine(string.Format("{0, 6:f4}", pointA.Translate(pointB)) + "  |  {0}", (pointA.Translate(pointB).X == 3 && pointA.Translate(pointB).Y == 3) ? "TEST OK" : "ERROR");
        }
    }
}
