﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OOP19_OverGit.utilities.math;

namespace OOP19_OverGit.riccardoOmiccioli.test.math
{
    static class TestVector2D
    {
        public static void Test()
        {
            Console.Write("Creating a new Vector2D with default coordinates          |  ");
            var vectorA = new Vector2D();
            Assert.IsTrue(vectorA.X == 0 && vectorA.Y == 0, "Exception in test create Vector2D");
            Console.WriteLine(vectorA + "  |  {0}", (vectorA.X == 0 && vectorA.Y == 0) ? "TEST OK" : "ERROR");

            Console.Write("Creating a new Vector2D with default coordinates [1 ; 1]  |  ");
            var vectorB = new Vector2D(new Point2D(1, 1));
            Assert.IsTrue(vectorB.X == 1 && vectorB.Y == 1, "Exception in test create Vector2D");
            Console.WriteLine(vectorB + "  |  {0}", (vectorB.X == 1 && vectorB.Y == 1) ? "TEST OK" : "ERROR");

            Console.Write("Setting Vector2D coordinates to [5 ; 10]                  |  ");
            vectorA.X = 5;
            vectorA.Y = 10;
            Assert.IsTrue(vectorA.X == 5 && vectorA.Y == 10, "Exception in set Vector2D coordinates");
            Console.WriteLine(vectorA + "  |  {0}", (vectorA.X == 5 && vectorA.Y == 10) ? "TEST OK" : "ERROR");

            Console.Write("Testing operator+  |  Correct result [  6 ; 11]  |  Actual result: ");
            Assert.IsTrue((vectorA + vectorB).X == 6 && (vectorA + vectorB).Y == 11, "Exception in Vector2D operator+");
            Console.WriteLine(vectorA + vectorB + "  |  {0}", ((vectorA + vectorB).X == 6 && (vectorA + vectorB).Y == 11) ? "TEST OK" : "ERROR");
            
            Console.Write("Testing operator-  |  Correct result [  4 ;  9]  |  Actual result: ");
            Assert.IsTrue((vectorA - vectorB).X == 4 && (vectorA - vectorB).Y == 9, "Exception in Vector2D operator-");
            Console.WriteLine(vectorA - vectorB + "  |  {0}", ((vectorA - vectorB).X == 4 && (vectorA - vectorB).Y == 9) ? "TEST OK" : "ERROR");

            Console.Write("Testing MultiplyByScalar  |  Correct result [10 ; 20]  |  Actual result: ");
            Assert.IsTrue(vectorA.MultiplyByScalar(2).X == 10 && vectorA.MultiplyByScalar(2).Y == 20, "Exception in Vector2D MultiplyByScalar");
            Console.WriteLine(vectorA.MultiplyByScalar(2) + "  |  {0}", (vectorA.MultiplyByScalar(2).X == 10 && vectorA.MultiplyByScalar(2).Y == 20) ? "TEST OK" : "ERROR");

            Console.Write("Testing Multiply  |  Correct result: 15  |  Actual result: ");
            Assert.IsTrue(vectorA.Multiply(vectorB) == 15, "Exception in Vector2D Multiply");
            Console.WriteLine(vectorA.Multiply(vectorB) + "  |  {0}", (vectorA.Multiply(vectorB) == 15) ? "TEST OK" : "ERROR");

            Console.Write("Testing Distance  |  Correct result [2 ; 0]  |  Actual result: ");
            vectorA.Distance(new Point2D(1, 0), new Point2D(3, 0));
            Assert.IsTrue(vectorA.X == 2 && vectorA.Y == 0, "Exception in Vector2D Distance");
            Console.WriteLine(vectorA + "  |  {0}", (vectorA.X == 2 && vectorA.Y == 0) ? "TEST OK" : "ERROR");

            Console.Write("Testing Angle  |  Correct result: 45 Deg  |  Actual result: ");
            Assert.IsTrue(Math.Truncate(vectorA.Angle(vectorB) * 180 / Math.PI) == 45, "Exception in Vector2D Angle");
            Console.WriteLine(string.Format("{0, 6:f2}", vectorA.Angle(vectorB) * 180 / Math.PI) + " Deg  |  {0}", (Math.Truncate(vectorA.Angle(vectorB) * 180 / Math.PI) == 45) ? "TEST OK" : "ERROR");

            Console.Write("Testing AngleOfThisVector  |  Correct result: 90 Deg  |  Actual result: ");
            Assert.IsTrue(Math.Truncate(vectorA.AngleOfThisVector() * 180 / Math.PI) == 90, "Exception in Vector2D AngleOfThisVector");
            Console.WriteLine(string.Format("{0, 6:f2}", vectorA.AngleOfThisVector() * 180 / Math.PI) + " Deg  |  {0}", (Math.Truncate(vectorA.AngleOfThisVector() * 180 / Math.PI) == 90) ? "TEST OK" : "ERROR");
        }
    }
}
