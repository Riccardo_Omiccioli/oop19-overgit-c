﻿using System;
using OOP19_OverGit.riccardoOmiccioli.test.math;

namespace OOP19_OverGit.riccardoOmiccioli.test
{
    static class TestAll
    {
        public static void StartTest()
        {
            Console.WriteLine("\nTEST POINT2D\n--------------------------------------------------------------------------------");
            TestPoint2D.Test();
            Console.WriteLine("\nTEST VECTOR2D\n--------------------------------------------------------------------------------");
            TestVector2D.Test();
        }
    }
}
