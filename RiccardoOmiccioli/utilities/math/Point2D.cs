﻿using System;

namespace OOP19_OverGit.utilities.math
{
    class Point2D : IPoint2D
    {
        public Point2D()
        {
            X = 0;
            Y = 0;
        }

        public Point2D(double x, double y)
        {
            X = x;
            Y = y;
        }

        public Point2D(Point2D point)
        {
            X = point.X;
            Y = point.Y;
        }

        public double X { get; set; }

        public double Y { get; set; }

        public static Point2D operator +(Point2D point1, Point2D point2)
        {
            return new Point2D(point1.X + point2.X, point1.Y + point2.Y);
        }

        public static Point2D operator -(Point2D point1, Point2D point2)
        {
            return new Point2D(point1.X - point2.X, point1.Y - point2.Y);
        }

        public static Point2D operator *(Point2D point, double value)
        {
            return new Point2D(point.X * value, point.Y * value);
        }

        public static Point2D operator /(Point2D point, double value)
        {
            return new Point2D(point.X / value, point.Y / value);
        }

        public double Distance(IPoint2D point)
        {
            return Math.Sqrt(Math.Pow(point.X - X, 2) + Math.Pow(point.Y - Y, 2));
        }

        public IPoint2D Translate(IPoint2D translation)
        {
            return new Point2D(X + translation.X, Y + translation.Y);
        }

        public override string ToString()
        {
            return string.Format("[{0, 6:f2} ; {1, 6:f2}]", X, Y);
        }
    }
}
