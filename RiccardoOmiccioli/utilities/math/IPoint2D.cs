﻿namespace OOP19_OverGit.utilities.math
{
    public interface IPoint2D
    {
        /**
         * Getter and setter for X value of the point.
         */
        double X { get; set; }

        /**
         * Getter and setter for X value of the point.
         */
        double Y { get; set; }

        /**
         * Translates the point by the specified translation.
         */
        IPoint2D Translate(IPoint2D translation);

        /**
         * Calculate and returns the distance between the point and the point passed to the method.
         */
        double Distance(IPoint2D point);
    }
}
