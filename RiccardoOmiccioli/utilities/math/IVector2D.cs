﻿using OOP19_OverGit.utilities.math;

namespace OOP19_OverGit
{
    public interface IVector2D
    {
        /**
         * Getter and setter for X value of the vector.
         */
        double X { get; set; }

        /**
         * Getter and setter for X value of the vector.
         */
        double Y { get; set; }
        
        /**
         * Returns a new Point2D containing the components of this vector.
         */
        IPoint2D Components();

        /**
         * Returns the module of the vector.
         */
        double Module();

        /**
         * Returns a new vector opposite of the vector.
         */
        IVector2D Opposite();

        /**
         * Sums the two vectors.
         */
        IVector2D Add(IVector2D vector);

        /**
         * Determines the difference between the first vector and the second vector.
         */
        // Default
        IVector2D Sub(IVector2D vector)
        {
            return this.Add(vector.Opposite());
        }

        /**
         * Scalar multiplication of the vector.
         */
        IVector2D MultiplyByScalar(double scalar);

        /**
         * Scalar multiplication between this vector and the vector passed as an argument.
         */
        double Multiply(IVector2D vector);

        /**
         * Sets the vector that goes from pointA to pointB (with sign).
         */
        void Distance(IPoint2D pointA, IPoint2D pointB);

        /**
         * Returns the angle between this vector and the vector passed as an argument.
         */
        double Angle(IVector2D vector);

        /**
         * Returns the angle of this vector in degrees and starting from "top" 
         * (in a Cartesian plane an angle of 0 degrees corresponds to the y axis).
         */
        double AngleOfThisVector();
    }
}
