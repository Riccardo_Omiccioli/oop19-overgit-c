﻿using System;
using OOP19_OverGit.utilities.math;

namespace OOP19_OverGit
{
    class Vector2D : IVector2D
    {
        public Vector2D(double module, double angle)
        {
            X = module * Math.Cos(angle);
            Y = module * Math.Sin(angle);
        }

        public Vector2D(Point2D pointA, Point2D pointB)
        {
            X = pointB.X - pointA.X;
            Y = pointB.Y - pointA.Y;
        }

        public Vector2D(IPoint2D components)
        {
            X = components.X;
            Y = components.Y;
        }

        public Vector2D() : this(new Point2D(0, 0))
        {
        }

        public double X { get; set; }

        public double Y { get; set; }

        public IPoint2D Components()
        {
            return new Point2D(X, Y);
        }

        public double Module()
        {
            return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
        }

        public IVector2D Opposite()
        {
            return new Vector2D(new Point2D(-X, -Y));
        }

        public IVector2D Add(IVector2D vector)
        {
            return new Vector2D(X + vector.X, Y + vector.Y);
        }
        
        public static Vector2D operator+(Vector2D vectorA, Vector2D vectorB)
        {
            return new Vector2D(new Point2D(vectorA.X + vectorB.X, vectorA.Y + vectorB.Y));
        }

        public static Vector2D operator-(Vector2D vectorA, Vector2D vectorB)
        {
            return new Vector2D(new Point2D(vectorA.X - vectorB.X, vectorA.Y - vectorB.Y));
        }

        public IVector2D MultiplyByScalar(double scalar)
        {
            return new Vector2D(new Point2D(X * scalar, Y * scalar));
        }

        public double Multiply(IVector2D vector)
        {
            return X * vector.X + Y * vector.Y;
        }

        public void Distance(IPoint2D pointA, IPoint2D pointB)
        {
            X = pointB.X - pointA.X;
            Y = pointB.Y - pointA.Y;
        }

        public double Angle(IVector2D vector)
        {
            return Math.Acos(Multiply(vector) / (Module() * vector.Module()));
        }

        public double AngleOfThisVector()
        {
            // Special cases when at least one component is 0
            if (X == 0)
            {
                return Y > 0 ? 0
                        : Y == 0 ? 0
                        : Math.PI;
            }
            else if (Y == 0)
            {
                return X > 0 ? Math.PI / 2
                        : Math.PI * 3 / 2;
            }
            // When both components are not 0
            double res = Math.Atan(Y / X);
            if (X < 0)
            {
                res = res + Math.PI;
            }
            else
            {
                res = res + Math.PI * 2;
            }
            return res % (Math.PI * 2);
        }

        public override string ToString()
        {
            return string.Format("[{0, 6:f2} ; {1, 6:f2}]", X, Y);
        }
    }
}
