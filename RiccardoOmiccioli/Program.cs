﻿using System;
using OOP19_OverGit.riccardoOmiccioli.test;

namespace OOP19_OverGit
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("OOP19-OverGit C# Main");

            TestAll.StartTest();
        }
    }
}
